
module.exports = {
  root: true,
  parserOptions: {
    parser: "babel-eslint"
  },
  env: {
    browser: true,
	webextensions: true,
	jest: true,
  },
  extends: [
    "plugin:vue/strongly-recommended",
    "standard"  ],
  plugins: [
	"vue",
	"jest"
  ],
  rules: {
    "generator-star-spacing": "off",
		"vue/no-use-v-if-with-v-for": "off",
		"vue/require-v-for-key": "off",
		"vue/html-indent": ["error", "tab"],
		"vue/html-quotes": ["error", "double"],
		"quotes": [2, "double"],
    "semi": [
			"error",
			"always"
		],
		"indent": [
			2,
			"tab"
		],
		"no-tabs": 0,
		"space-before-function-paren": 0,
		"comma-dangle": [
			"error",
			"always-multiline"
		],
		"standard/no-callback-literal": 1
  }
}
