# chat-status

Chat status notify extension.

# Download test version

[chat-status-v1.0.0.zip]()

How to install:

* extract the extension .zip in it's own directory (create a directory manually prior to extracting or via archive manager automatically)

* in chrome go to the 3 dot button and open options, then go to more tools >> Extensions, extension page should open.

* on extension page enable developer mode (a slider button on the top right corner)

* once developer mode is enabled you should see a button called "Load unpacked" press it and navigate to where you extracted the extension .zip and load that directory.


## Requirements

- Node.js >= 8 and npm >= 5
- [git](https://git-scm.com)
- [vue-cli 2](https://github.com/vuejs/vue-cli/tree/v2)

## Usage

```bash
git clone https://gitlab.com/bvujnovac/chat-status.git
$ cd chat-status
$ npm install
$ npm run build
```

### `npm run build`

Build the extension into `dist` folder for **production**.

### `npm run build:dev`

Build the extension into `dist` folder for **development**.

### `npm run watch`

Watch for modifications then run `npm run build`.

### `npm run watch:dev`

Watch for modifications then run `npm run build:dev`.

### `npm run build-zip`

Build a zip file following this format `<name>-v<version>.zip`, by reading `name` and `version` from `manifest.json` file.
Zip file is located in `dist-zip` folder.

### `npm run lint`

Launch linter

