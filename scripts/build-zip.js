#!/usr/bin/env node

const fs = require("fs");
const path = require("path");
const archiver = require("archiver");
const crypto = require("crypto");

const DEST_DIR = path.join(__dirname, "../dist");
const DEST_ZIP_DIR = path.join(__dirname, "../dist-zip");

const extractExtensionData = () => {
	const extPackageJson = require("../package.json");

	return {
		name: extPackageJson.name,
		version: extPackageJson.version,
	};
};

const makeDestZipDirIfNotExists = () => {
	if (!fs.existsSync(DEST_ZIP_DIR)) {
		fs.mkdirSync(DEST_ZIP_DIR);
	}
};

const buildZip = (src, dist, zipFilename) => {
	console.info(`Building ${zipFilename}...`);

	const archive = archiver("zip", { zlib: { level: 9 } });
	const stream = fs.createWriteStream(path.join(dist, zipFilename));

	return new Promise((resolve, reject) => {
		archive
			.directory(src, false)
			.on("error", err => reject(err))
			.pipe(stream);

		stream.on("close", () => resolve());
		archive.finalize();
	});
};

const main = () => {
	const { name, version } = extractExtensionData();
	const zipFilename = `${name}-v${version}.zip`;

	makeDestZipDirIfNotExists();

	buildZip(DEST_DIR, DEST_ZIP_DIR, zipFilename)
		.then(() => console.info("OK"))
		.catch(console.err);

	const algo = "sha1";
	var shasum = crypto.createHash(algo);
	var file = `${DEST_ZIP_DIR}/${zipFilename}`;
	var s = fs.ReadStream(file);
	s.on("data", function(d) { shasum.update(d); });
	s.on("end", function() {
		var d = shasum.digest("hex");
		console.log(d);
		fs.writeFile(`${DEST_ZIP_DIR}/${zipFilename}.sha1`, d, (err) => {
			if (err) console.log(err);
			console.log("Successfully written to File.");
		});
	});
};
main();
