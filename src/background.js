import io from "socket.io-client";
import Notify from "notifyjs";
const ipaddr = require("ipaddr.js");
const notifyAudio = new Audio("/sounds/ping.mp3");
const notifyAudioWallboardDown = new Notify("Wallboard might be down!!!", {
	body: "",
	tag: "wallboardDown",
	timeout: 5,
});
let configuration = {};
notifyAudio.volume = 0.1; // adjusting sound notification volume

/* istanbul ignore if */
if (typeof exports !== "object") {
	init();
}

async function getKeys() {
	const keys = await browser.storage.local.get(["keyPrimaryDepartment", "keySecondaryDepartment", "keyIsSecondaryDepartmentOn"]);
	return keys;
}
function playSound () {
	notifyAudio.play();
	return true;
}

function handleNotification(chats, department) {
	configuration.currentDepartment = department;
	const notifyAudioChats = new Notify("Chaaats queued!!!", {
		body: configuration.currentDepartment,
		tag: configuration.currentDepartment,
		notifyShow: playSound,
		timeout: 2,
	});
	if (chats.open > chats.limit) {
		notifyAudioChats.show();
		return true;
	}
	console.log("it works on: ", configuration.currentDepartment, "chats are: ", chats);
}

async function handleDown() {
	const ip = await fetch("https://ip.somecompany.com/").then(res => res.text());
	const isPrivate = ipaddr.parse(ip).range();
	if (isPrivate === "private") {
		notifyAudioWallboardDown.show();
	}
	return isPrivate;
}

async function handleMessage (action) {
	const { type, payload } = action;
	switch (type) {
	case "refreshSocketData":
		socketData();
		return "done";
	case "setPrimaryDepartmentKey":
		configuration.primaryDepartment = payload;
		try {
			await browser.storage.local.set({ "keyPrimaryDepartment": payload });
		} catch (e) {}
		return configuration.primaryDepartment;
	case "setSecondaryDepartmentKey":
		configuration.secondaryDepartment = payload;
		try {
			await browser.storage.local.set({ "keySecondaryDepartment": payload });
		} catch (e) {}
		return configuration.secondaryDepartment;
	case "keyIsSecondaryDepartmentOn":
		configuration.isSecondaryDepartmentOn = payload;
		try {
			await browser.storage.local.set({ "keyIsSecondaryDepartmentOn": payload });
		} catch (e) {}
		return configuration.isSecondaryDepartmentOn;
	}
}

async function socketData() {
	try {
		const results = await getKeys();
		configuration.primaryDepartment = await results.keyPrimaryDepartment;
		configuration.secondaryDepartment = await results.keySecondaryDepartment;
		configuration.isSecondaryDepartmentOn = await results.keyIsSecondaryDepartmentOn;
	} catch (e) {}
	socket.emit("data.off", true);
	socket.emit("data.on", [`${configuration.primaryDepartment}.chats`]);
	if (configuration.isSecondaryDepartmentOn) {
		socket.emit("data.on", [`${configuration.secondaryDepartment}.chats`]);
	}
	socket.on(`data.${configuration.primaryDepartment}.chats`, function Chats (chats) {
		handleNotification(chats, configuration.primaryDepartment);
	});
	if (configuration.isSecondaryDepartmentOn) {
		socket.on(`data.${configuration.secondaryDepartment}.chats`, function Chats (chats) {
			handleNotification(chats, configuration.secondaryDepartment);
		});
	}
	return "done";
}
async function init() {
	try {
		const result = await getKeys();

		if (result.keyIsSecondaryDepartmentOn === undefined) {
			browser.storage.local.set({ "keyIsSecondaryDepartmentOn": false });
		} else {
			configuration.isSecondaryDepartmentOn = result.keyIsSecondaryDepartmentOn;
		}
		if (result.keyPrimaryDepartment === undefined) {
			browser.storage.local.set({ "keyPrimaryDepartment": "support" });
		} else {
			configuration.primaryDepartment = result.keyPrimaryDepartment;
		}
		if (result.keySecondaryDepartment === undefined) {
			browser.storage.local.set({ "keySecondaryDepartment": "maps" });
		} else {
			configuration.secondaryDepartment = result.keySecondaryDepartment;
		}
	} catch (e) {}
	browser.runtime.onMessage.addListener(handleMessage);
	return "done";
}

const socket = io("https://somecompany.com", { upgrade: false, transports: ["websocket"] });
socket.on("connect", socketData);
socket.on("connect_error", handleDown);

export {
	getKeys,
	playSound,
	handleNotification,
	handleDown,
	handleMessage,
	socketData,
	init,
};
