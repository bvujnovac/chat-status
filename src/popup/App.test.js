import { shallowMount } from "@vue/test-utils";
import Component from "./App.vue";
jest.mock("webextension-polyfill", () => require("sinon-chrome/webextensions"));

const storageGet = jest.fn(() => ({ keyPrimaryDepartment: "support", keySecondaryDepartment: "maps", keyIsSecondaryDepartmentOn: true }));
const storageSet = jest.fn();
const onMessage = jest.fn();
const runtimeSendMessage = jest.fn();

global.browser = {
	storage: {
		local: {
			get: storageGet,
			set: storageSet,
		},
	},
	runtime: {
		sendMessage: runtimeSendMessage,
		onMessage: {
			addListener: onMessage,
		},
	},
};

let wrapper;

beforeEach(() => {
	wrapper = shallowMount(Component, {
		propsData: {},
		mocks: {},
		stubs: {},
		methods: {},
		attachToDocument: false,
	});
	jest.clearAllMocks();
});

afterEach(() => {
	wrapper.destroy();
});

jest.useFakeTimers();

describe("App.vue", () => {
	describe("App", () => {
		it("is a Vue instance?", () => {
			expect(wrapper.isVueInstance()).toBeTruthy();
		});
	});

	describe("Enable secondary department", () => {
		it("checking the box on Enable secondary department checkbox", async () => {
			const checkboxInput = await wrapper.find("input[type=\"checkbox\"]");
			checkboxInput.setChecked(false);
			expect(wrapper.vm.isSecondaryDepartmentOn).toBe(false);
		});
	});

	describe("Choose primary department", () => {
		it("selecting primary department", async () => {
			const radioInput = await wrapper.find("input[type=\"radio\"][value=\"support\"]");
			radioInput.setChecked();
			expect(wrapper.vm.primaryDepartment).toBe("support");
		});
	});

	describe("Choose secondary department", () => {
		it("selecting secondary department", async () => {
			const radioInputArray = await wrapper.findAll("input[type=\"radio\"][value=\"maps\"]");
			const radioInput = radioInputArray.at(1);
			radioInput.setChecked(true);
			expect(wrapper.vm.secondaryDepartment).toBe("maps");
		});
	});
});
