// import SocketMock from "socket.io-mock";
global.fetch = require("jest-fetch-mock");
jest.mock("notifyjs");

const storageGet = jest.fn(() => ({ keyPrimaryDepartment: "support", keySecondaryDepartment: "maps", keyIsSecondaryDepartmentOn: true }));
const storageSet = jest.fn();
const onMessage = jest.fn();
const chats = { "queue": 0, "queue_time": 0, "active": 6, "active_time": 20, "open": 6, "limit": 13 };
const chatsTrue = { "queue": 0, "queue_time": 0, "active": 6, "active_time": 20, "open": 6, "limit": 5 };
// let socket = new SocketMock();

global.browser = {
	storage: {
		local: {
			get: storageGet,
			set: storageSet,
		},
	},
	runtime: {
		onMessage: {
			addListener: onMessage,
		},
	},
};

window.HTMLMediaElement.prototype.play = () => {};

const {
	getKeys,
	playSound,
	handleNotification,
	handleDown,
	handleMessage,
	socketData,
	init,
} = require("./background");

beforeEach(() => {
	jest.clearAllMocks();
});

describe("background.js", () => {
	describe("getKeys", () => {
		it("should return stored data", async () => {
			var result = await getKeys();
			expect(result.keyPrimaryDepartment).toEqual("support");
			expect(result.keySecondaryDepartment).toEqual("maps");
			expect(result.keyIsSecondaryDepartmentOn).toBeDefined();
		});
	});

	describe("test playSound function", () => {
		it("should work", () => {
			const result = playSound();
			expect(result).toBeTruthy();
		});
	});

	describe("handleNotification", () => {
		it("false", () => {
			var result = handleNotification(chats, "support");
			expect(result).toBeFalsy();
		});
		it("true", () => {
			var result = handleNotification(chatsTrue, "support");
			expect(result).toBeTruthy();
		});
	});

	describe("handleDown", () => {
		beforeEach(() => {
			fetch.resetMocks();
		});

		it("true", async () => {
			fetch.mockResponseOnce("10.20.45.161");
			var result = await handleDown();
			expect(result).toEqual("private");
		});
		it("false", async () => {
			fetch.mockResponseOnce("8.8.8.8");
			var result = await handleDown();
			expect(result).toEqual("unicast");
		});
	});

	describe("socketData", () => {
		it("should work", async () => {
			var result = await socketData();
			expect(result).toEqual("done");
		});
		it("else path", async () => {
			storageGet.mockReturnValueOnce({ keyPrimaryDepartment: undefined, keySecondaryDepartment: undefined, keyIsSecondaryDepartmentOn: undefined });
			var result = await socketData();
			expect(result).toEqual("done");
		});
	});

	describe("handleMessage ", () => {
		it("getData switch", async () => {
			var result = await handleMessage({
				type: "refreshSocketData",
			});
			expect(result).toEqual("done");
		});
		it("keyPrimaryDepartment switch", async () => {
			var result = await handleMessage({
				type: "setPrimaryDepartmentKey",
				payload: "support",
			});
			expect(result).toEqual("support");
		});
		it("keySecondaryDepartment switch", async () => {
			var result = await handleMessage({
				type: "setSecondaryDepartmentKey",
				payload: "maps",
			});
			expect(result).toEqual("maps");
		});
		it("keyIsSecondaryDepartmentOn switch", async () => {
			var result = await handleMessage({
				type: "keyIsSecondaryDepartmentOn",
				payload: false,
			});
			expect(result).toEqual(false);
		});
	});

	describe("init", () => {
		beforeEach(() => {
			fetch.resetMocks();
		});

		it("init test if", async () => {
			storageGet.mockReturnValueOnce({ keyPrimaryDepartment: undefined, keySecondaryDepartment: undefined, keyIsSecondaryDepartmentOn: undefined });
			fetch.mockResponseOnce("10.20.45.161");
			const result = await init();
			expect(result).toEqual("done");
		});

		it("init test else", async () => {
			storageGet.mockReturnValueOnce({ keyPrimaryDepartment: "support", keySecondaryDepartment: "maps", keyIsSecondaryDepartmentOn: true });
			fetch.mockResponseOnce("10.20.45.161");
			const result = await init();
			expect(result).toEqual("done");
		});
	});
});
